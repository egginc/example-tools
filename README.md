# example-tools

Egg, Inc examples - how to use the api.

Prerequisites

* Protobuf
* Requests

Install both with `python3 -m pip install -r requirements.txt`

Run with:

```
EGGINC_ID="<YOUR EID>" python3 ./generate_data_files.py
```

This will create:
```
first_contact.json
artifacts.json
config.json
periodicals.json
```

`ei.py` and `ei_pb2.py` copied from https://github.com/thenicopanda/Egg-Inc.-Python-API.