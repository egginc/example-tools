from ei import *
import json
import os

# Generates a bunch of json files with all your data in

ei_user_id = os.getenv('EGGINC_ID')

with open('first_contact.json', 'w+') as f:
    r = get_first_contact(ei_user_id)
    f.write(json.dumps(r, indent=2))

with open('periodicals.json', 'w+') as f:
    r = get_periodicals(ei_user_id)
    f.write(json.dumps(r, indent=2))


with open('config.json', 'w+') as f:
    r = get_config(ei_user_id)
    f.write(json.dumps(r, indent=2))


with open('artifacts.json', 'w+') as f:
    r = get_afx_config(ei_user_id)
    f.write(json.dumps(r, indent=2))
